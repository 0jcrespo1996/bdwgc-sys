use std::env;

const LIB_ATOMIC_OPS_DIR: &str = "vendor/libatomic_ops";
const LIB_GC_DIR: &str = "vendor/bdwgc";
const LIB_GC_HEADERS_DIR: &str = "vendor/bdwgc/include";
fn main() {
    use cmake::Config;
    use std::path::Path;

    let libatomic_include_path = Path::new(LIB_ATOMIC_OPS_DIR)
        .join("src")
        .canonicalize()
        .unwrap()
        .display()
        .to_string()
        .replace(r"\\?\", "");

    let dst = Config::new(LIB_GC_DIR)
        .profile("Release")
        .define("BUILD_SHARED_LIBS", "FALSE")
        .cflag(format!("-I{}", libatomic_include_path))
        .build();

    println!(
        "cargo:rustc-link-search=native={}",
        dst.join("lib").display()
    );

    let bindings = bindgen::builder()
        .header(Path::new(LIB_GC_HEADERS_DIR).join("gc.h").to_str().unwrap())
        .generate()
        .unwrap();

    let out_path = env::var("OUT_DIR").unwrap();
    let out_dir = Path::new(out_path.as_str());
    bindings
        .write_to_file(out_dir.join("bindings.rs"))
        .expect("Couldn't write bindings!");
    println!("cargo:rustc-link-lib=static=gc");
}